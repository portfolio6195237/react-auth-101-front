import axios from 'axios';

export const API = axios.create({
  baseURL: 'https://auth-2024-bb82805d7782.herokuapp.com',
});
