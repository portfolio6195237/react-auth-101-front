## Аутентификация пользователя

**🛠️ Стек**

React + TypeScript + CSS

**💬 Основная информация о проекте**

Frontend часть приложения на основе LinkedIn курса "React: Authentication":
https://www.linkedin.com/learning/react-authentication

Основные возможности приложения:
* аутентификация на базе JWT токена;
* вход при помощи Google аккаунта;
* верификация почты;
* сброс пароля;
* шифрование пароля;
* обработка ошибок;
* защита БД.

Так как курс 2021 года, заменены версии библиотек на последние и использован TS, вместо JS. Также сборщик CRA заменён на Webpack. Соответственно реализации решений изменены, с учётом возможностей и требований современных технологий.

Backend часть приложения: https://github.com/yuriyvyatkin/react-auth-101-back

**📚 Инструкция по запуску**

##### 1. Клонирование

```
git clone https://github.com/yuriyvyatkin/react-auth-101-front.git
```

```
cd react-auth-101-front
```

##### 2. Запуск

```
npm i && npm run start
```

или

```
npm install
```

```
npm run start
```
